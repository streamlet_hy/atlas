%define enable_native_atlas 0
%if "%{?enable_native_atlas}" != "0"
%define dist .native
%endif

%undefine _strict_symbol_defs_build

Summary:        Automatically Tuned Linear Algebra Software
Name:           atlas
Version:        3.10.3
Release:        11%{?dist}
License:        BSD
URL:            http://math-atlas.sourceforge.net/
Source0:        http://downloads.sourceforge.net/math-atlas/%{name}%{version}.tar.bz2
Patch3000:	atlas-melf.patch
Patch3001:	atlas-throttling.patch
Patch3002:	atlas-genparse.patch
Patch3003:	atlas.3.10.1-unbundle.patch
Patch3004:	atlas-gcc10.patch
Patch3005:	atlas-getri.patch
Patch3006:	atlas-fgrep.patch
Patch3030:	atlas-shared_libraries.patch
Patch3031:	0001-add-loongarch64-support-for-atlas.patch
Patch3032:	0001-modify-atlas-build-error-on-loongarch64.patch
Patch3033:	0001-add-a-missing-loongarch-macro-definition.patch

BuildRequires:  make gcc gcc-gfortran lapack-static

%description
The ATLAS (Automatically Tuned Linear Algebra Software) project is an
ongoing research effort f(ocusing on applying empirical techniques in
order to provide portable performance. At present, it provides C and
Fortran77 interfaces to a portably efficient BLAS implementation, as
well as a few routines from LAPACK.

The performance improvements in ATLAS are obtained largely via
compile-time optimizations and tend to be specific to a given hardware
configuration. In order to package ATLAS some compromises
are necessary so that good performance can be obtained on a variety
of hardware. This set of ATLAS binary packages is therefore not
necessarily optimal for any specific hardware configuration.  However,
the source package can be used to compile customized ATLAS packages;
see the documentation for information.


%package devel
Summary:        Development libraries for ATLAS
Requires:       %{name} = %{version}-%{release}
Requires(posttrans):	/usr/sbin/alternatives
Requires(postun):	/usr/sbin/alternatives

%description devel
This package contains headers for development with ATLAS
(Automatically Tuned Linear Algebra Software).


%package static
Summary:        Static libraries for ATLAS
Requires:       %{name}-devel = %{version}-%{release}
Requires(posttrans):	/usr/sbin/alternatives
Requires(postun):	/usr/sbin/alternatives

%description static
This package contains static version of ATLAS (Automatically Tuned
Linear Algebra Software).


%define types base

%if "%{?enable_native_atlas}" == "0"
%ifarch x86_64
%define types base corei2

%package corei2-static
Summary:        ATLAS libraries for Corei2 (Ivy/Sandy bridge) CPUs

%description corei2-static
This package contains the ATLAS (Automatically Tuned Linear Algebra
Software) static libraries compiled with optimizations for the Corei2 (Ivy/Sandy bridge)
CPUs. The base ATLAS builds for the x86_64 architecture are made for the hammer64 CPUs.

%package corei2
Summary:        ATLAS libraries for Corei2 (Ivy/Sandy bridge) CPUs

%description corei2
This package contains the ATLAS (Automatically Tuned Linear Algebra
Software) libraries compiled with optimizations for the Corei2 (Ivy/Sandy bridge)
CPUs. The base ATLAS builds for the x86_64 architecture are made for the hammer64 CPUs.


%package corei2-devel
Summary:        Development libraries for ATLAS for Corei2 (Ivy/Sandy bridge) CPUs
Requires:       %{name}-corei2 = %{version}-%{release}
Requires(posttrans):	/usr/sbin/alternatives
Requires(postun):	/usr/sbin/alternatives

%description corei2-devel
This package contains shared and static versions of the ATLAS
(Automatically Tuned Linear Algebra Software) libraries compiled with
optimizations for the corei2 (Ivy/Sandy bridge) CPUs.
%endif
%endif


%prep
%setup -q -n ATLAS
%autopatch -p1 -m 3000 -M 3006
%patch3030 -p2
%patch3031 -p1
%patch3032 -p1
%patch3033 -p1


# Generate lapack library
mkdir lapacklib
cd lapacklib
ar x %{_libdir}/liblapack_pic.a
# Remove functions that have ATLAS implementations
rm -f cgelqf.f.o cgels.f.o cgeqlf.f.o cgeqrf.f.o cgerqf.f.o cgesv.f.o cgetrf.f.o cgetri.f.o cgetrs.f.o clarfb.f.o clarft.f.o clauum.f.o cposv.f.o cpotrf.f.o cpotri.f.o cpotrs.f.o ctrtri.f.o dgelqf.f.o dgels.f.o dgeqlf.f.o dgeqrf.f.o dgerqf.f.o dgesv.f.o dgetrf.f.o dgetri.f.o dgetrs.f.o dlamch.f.o dlarfb.f.o dlarft.f.o dlauum.f.o dposv.f.o dpotrf.f.o dpotri.f.o dpotrs.f.o dtrtri.f.o ieeeck.f.o ilaenv.f.o lsame.f.o sgelqf.f.o sgels.f.o sgeqlf.f.o sgeqrf.f.o sgerqf.f.o sgesv.f.o sgetrf.f.o sgetri.f.o sgetrs.f.o slamch.f.o slarfb.f.o slarft.f.o slauum.f.o sposv.f.o spotrf.f.o spotri.f.o spotrs.f.o strtri.f.o xerbla.f.o zgelqf.f.o zgels.f.o zgeqlf.f.o zgeqrf.f.o zgerqf.f.o zgesv.f.o zgetrf.f.o zgetri.f.o zgetrs.f.o zlarfb.f.o zlarft.f.o zlauum.f.o zposv.f.o zpotrf.f.o zpotri.f.o zpotrs.f.o ztrtri.f.o 
# Create new library
ar rcs ../liblapack_pic_pruned.a *.o
cd ..


%build
p=$(pwd)
%global mode -b %{__isa_bits}
%define arg_options %{nil}
%define flags %{nil}
%define threads_option "-t 2"

%ifarch x86_64
%define flags %{nil}
%define base_options "-A HAMMER -V 896"
%endif

%ifarch ppc64le
%define flags %{nil}
%define base_options "-A POWER8 -V 1"
%endif

%ifarch aarch64
%define flags %{nil}
%define base_options "-A ARM64a53 -V 1"
%endif

%ifarch loongarch64
%define flags %{nil}
%define base_options "-A LOONGARCH64 -V 1"
%endif

%if "%{?enable_native_atlas}" != "0"
%define	threads_option %{nil}
%define base_options %{nil}
%define flags %{nil}
%endif

for type in %{types}; do
	if [ "$type" = "base" ]; then
		libname=atlas
		arg_options=%{base_options}
		thread_options=%{threads_option}
		%define pr_base %(echo $((%{__isa_bits}+0)))
	else
		libname=atlas-${type}
		if [ "$type" = "corei2" ]; then
			thread_options="-t 4"
			arg_options="-A Corei2 -V 896"
			%define pr_corei2 %(echo $((%{__isa_bits}+2)))
		fi
	fi
	mkdir -p %{_arch}_${type}
	pushd %{_arch}_${type}
	../configure  %{mode} $thread_options $arg_options -D c -DWALL -F xc ' '  -Fa alg '%{flags} -D_FORTIFY_SOURCE=2 -g -Wa,--noexecstack,--generate-missing-build-notes=yes -fstack-protector-strong -fstack-clash-protection -fPIC -Wl,-z,now'\
	--prefix=%{buildroot}%{_prefix}			\
	--incdir=%{buildroot}%{_includedir}		\
	--libdir=%{buildroot}%{_libdir}/${libname}

	sed -i "s#SLAPACKlib.*#SLAPACKlib = ${p}/liblapack_pic_pruned.a#" Make.inc
	cat Make.inc

	make build
	cd lib
	make shared
	make ptshared
	popd
done


%install
for type in %{types}; do
	pushd %{_arch}_${type}
	make DESTDIR=%{buildroot} install
        mv %{buildroot}%{_includedir}/atlas %{buildroot}%{_includedir}/atlas-%{_arch}-${type}
        mv %{buildroot}%{_includedir}/clapack.h %{buildroot}%{_includedir}/atlas-%{_arch}-${type}/clapack.h
        mv %{buildroot}%{_includedir}/cblas.h %{buildroot}%{_includedir}/atlas-%{_arch}-${type}/cblas.h
	if [ "$type" = "base" ]; then
		cp -pr lib/*.so* %{buildroot}%{_libdir}/atlas/
		rm -f %{buildroot}%{_libdir}/atlas/*.a
		cp -pr lib/libcblas.a lib/libatlas.a lib/libf77blas.a lib/liblapack.a %{buildroot}%{_libdir}/atlas/
	else
		cp -pr lib/*.so* %{buildroot}%{_libdir}/atlas-${type}/
		rm -f %{buildroot}%{_libdir}/atlas-${type}/*.a
		cp -pr lib/libcblas.a lib/libatlas.a lib/libf77blas.a lib/liblapack.a %{buildroot}%{_libdir}/atlas-${type}/
	fi
	popd

	mkdir -p %{buildroot}/etc/ld.so.conf.d
	if [ "$type" = "base" ]; then
		echo "%{_libdir}/atlas"		\
		> %{buildroot}/etc/ld.so.conf.d/atlas-%{_arch}.conf
	else
		echo "%{_libdir}/atlas-${type}"	\
		> %{buildroot}/etc/ld.so.conf.d/atlas-%{_arch}-${type}.conf
	fi
done

mkdir -p $RPM_BUILD_ROOT%{_libdir}/pkgconfig/
cat > $RPM_BUILD_ROOT%{_libdir}/pkgconfig/atlas.pc << DATA
Name: %{name}
Version: %{version}
Description: %{summary}
Cflags: -I%{_includedir}/atlas/
Libs: -L%{_libdir}/atlas/ -lsatlas
DATA


mkdir -p %{buildroot}%{_includedir}/atlas


%check
for type in %{types}; do
	pushd %{_arch}_${type}
	make check ptcheck
	popd
done

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%posttrans devel
/usr/sbin/alternatives	--install %{_includedir}/atlas atlas-inc 	\
		%{_includedir}/atlas-%{_arch}-base %{pr_base}

%postun devel
if [ $1 -ge 0 ] ; then
/usr/sbin/alternatives --remove atlas-inc %{_includedir}/atlas-%{_arch}-base
fi


%if "%{?enable_native_atlas}" == "0"

%ifarch x86_64
%post -n atlas-corei2 -p /sbin/ldconfig

%postun -n atlas-corei2 -p /sbin/ldconfig

%posttrans corei2-devel
	/usr/sbin/alternatives	--install %{_includedir}/atlas atlas-inc 	\
		%{_includedir}/atlas-%{_arch}-corei2  %{pr_corei2}

%postun corei2-devel
if [ $1 -ge 0 ] ; then
	/usr/sbin/alternatives --remove atlas-inc %{_includedir}/atlas-%{_arch}-corei2
fi
%endif

%endif


%files
%config(noreplace) /etc/ld.so.conf.d/atlas-%{_arch}.conf
%dir %{_libdir}/atlas
%{_libdir}/atlas/*.so.*

%files devel
%doc doc
%{_libdir}/atlas/*.so
%{_libdir}/pkgconfig/atlas.pc
%{_includedir}/atlas-%{_arch}-base/
%ghost %{_includedir}/atlas

%files static
%{_libdir}/atlas/*.a


%if "%{?enable_native_atlas}" == "0"

%ifarch x86_64
%files corei2
%config(noreplace) /etc/ld.so.conf.d/atlas-%{_arch}-corei2.conf
%dir %{_libdir}/atlas-corei2
%{_libdir}/atlas-corei2/*.so.*

%files corei2-devel
%doc doc
%{_libdir}/atlas-corei2/*.so
%{_includedir}/atlas-%{_arch}-corei2/
%ghost %{_includedir}/atlas

%files corei2-static
%{_libdir}/atlas-corei2/*.a
%endif

%endif


%changelog
* Thu Dec 19 2024 Huang Yang <huangyang@loongson.cn> - 3.10.3-11
- [Type] other
- [DESC] Add a missing loongarch macro definition

* Tue Oct 15 2024 Huang Yang <huangyang@loongson.cn> - 3.10.3-10
- [Type] other
- [DESC] Fix build error on loongarch64

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.10.3-9
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.10.3-8
- Rebuilt for loongarch release

* Tue Mar 19 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 3.10.3-7
- add loongarch64 support for atlas

* Mon Sep 11 2023 Miaojun Dong <zoedong@tencent.com> - 3.10.3-6
- Replace fgrep with grep -F to fix assertion failure

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.10.3-5
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.10.3-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.10.3-3
- Rebuilt for OpenCloudOS Stream 23

* Tue Jan 10 2023 rockerzhu <rockerzhu@tencent.com> - 3.10.3-2
- Delete ix86

* Thu Nov 24 2022 rockerzhu <rockerzhu@tencent.com> - 3.10.3-1
- Initial build
